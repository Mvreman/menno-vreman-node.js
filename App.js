const express = require("express");
const config = require("./config/app.json");
const site = require("./routes/router");
const session = require("express-session");
const app = express();
const port = 8080;

const prefix = config.enviroment === "dev" ? "" : "/~menno/conferenceapp/";

app.set("view engine", "ejs");
app.set("views", `${process.cwd()}/resources/views`);

//session
app.set('trust proxy', 1);
app.use(session({
    secret: 'keybaord cat',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: 'auto'}
    })
);

app.use(function (req, res, next) {
    res.locals.userObject = req.session.userObject;
    next();
});


//routing
app.use(express.static('public'));

app.use(`${prefix}/`, site);

app.listen(port, () => {
    console.log(`Website on ${port}`);
});
