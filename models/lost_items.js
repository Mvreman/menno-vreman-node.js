'use strict';
module.exports = (sequelize, DataTypes) => {
  const lost_items = sequelize.define('lost_items', {
    object: DataTypes.STRING,
    place: DataTypes.STRING,
    date: DataTypes.DATE,
    description: DataTypes.STRING
  }, {});
  lost_items.associate = function(models) {
    // associations can be defined here
  };
  return lost_items;
};