const express = require("express");
const controller = require("../controller/index");
const bodyParser = require("body-parser");

const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));

router
    //home
    .get("/", controller.HomeController.index)
    //auth
    .get("/register", controller.UserController.index)
    .get("/login", controller.InlogController.index)
    .get("/userdashboard", controller.UserDashboardController.index)
    .post("/register", controller.UserController.add)
    .post("/login", controller.InlogController.auth)
    .get("/uitloggen", controller.UserDashboardController.destroy)
    .post("/userdashboard", controller.UserDashboardController.add)
    .get("/voorwerpen", controller.VoorwerpenController.index)
    .post("/voorwerpen/:lost_items", controller.VoorwerpenController.remove);

module.exports = router;
