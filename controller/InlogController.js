const models = require("../models");
const bcrypt = require("bcrypt");
const moment = require("moment");
const config = require("../config/app.json");

const index = (req, res) => {
    models.users.findByPk(req.session.userObject)
    .then(user => {
        res.render("auth/inlog")
    })
};

const auth = (req, res) => {
    console.log(req.body);
    models.users.findOne({
        where:{
            email: req.body.email
        }
    }).then(user => {
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (result) {
                 req.session.userObject ={
                     id: user.id,
                     name: user.name,
                     email: user.email,
                     date: moment(user.createdAt).format(' Do MMMM YYYY,')
                 };
                res.redirect("/userdashboard");
            } else {
                res.send("Fout wachtwoord")
            }
        });
    }).catch(err => {
        res.send(err);
        res.render("/login")
    })
};

module.exports = {
    index, auth
};