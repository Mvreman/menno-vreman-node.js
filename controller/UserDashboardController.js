const models = require("../models");

const index = (req, res) => {
    models.users.findByPk(req.session.userObject.id)
        .then(user => {
            res.render("dashboard/userdashboard", {
                user:user,

            })
        }).catch(err => {
        req.session.userObject = null;
        res.render("/login")
    })
};

const add = (req, res) => {

    models.lost_items.create({

        object: req.body.object,
        place: req.body.place,
        date: req.body.date,
        description: req.body.description
    }).then(user => {
        res.redirect("/voorwerpen")
    }).catch(err => {
        res.send(err)
    })
};

const destroy = (req, res ) => {
    req.lost_items.findByPk();
    res.redirect('/');
};

module.exports = {
    index, destroy, add
};