const UserController = require("./UserController");
const HomeController = require("./HomeController");
const InlogController = require("./InlogController");
const UserDashboardController =require("./UserDashboardController");
const VoorwerpenController =require("./VoorwerpenController");



module.exports = {
    UserController: UserController,
    HomeController: HomeController,
    InlogController: InlogController,
    UserDashboardController: UserDashboardController,
    VoorwerpenController:VoorwerpenController,
};
