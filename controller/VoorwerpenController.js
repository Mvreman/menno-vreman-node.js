const models = require("../models/");
const config = require("../config/app.json");
const moment = require("moment");

const index = (req, res) => {
    models.lost_items.findAll().then(lost_items => {
        res.render("dashboard/voorwerpen", {
            lost_items:lost_items,
            moment:moment
        })
    }).catch(err => {
        console.log(err);
        res.end()
    });
};
const remove = (req, res) => {
    models.lost_items.destroy({
        where: {
            id: req.body.id
        }
    }).then(lost_items => {
        res.redirect("/voorwerpen")
    }).catch(err => {
        res.send(err)
    });
};


module.exports = {
    index, remove
};